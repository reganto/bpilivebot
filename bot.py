import os
import requests
import telebot

bot = telebot.TeleBot(os.environ.get('API_TOKEN'))

@bot.message_handler(commands=['start'])
def start(message):
    msg = 'Hi! I am bpi bot\nMy commands -> /bpidollar, /bpipound, /bpieuro :)'
    bot.send_message(message.chat.id, msg)


@bot.message_handler(commands=['bpidollar'])
def bpidollar(message):
    response = requests.get('https://api.coindesk.com/v1/bpi/currentprice.json')
    response = response.json()
    bot.send_message(message.chat.id, response['bpi']['USD']['rate']+' '+'$')


@bot.message_handler(commands=['bpipound'])
def bpipound(message):
    response = requests.get('https://api.coindesk.com/v1/bpi/currentprice.json')
    response = response.json()
    bot.send_message(message.chat.id, response['bpi']['GBP']['rate']+' '+'£')


@bot.message_handler(commands=['bpieuro'])
def bpieuro(message):
    response = requests.get('https://api.coindesk.com/v1/bpi/currentprice.json')
    response = response.json()
    bot.send_message(message.chat.id, response['bpi']['EUR']['rate']+' '+'€')


bot.polling()

